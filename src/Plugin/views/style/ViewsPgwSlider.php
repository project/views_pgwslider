<?php

/**
 * @file
 * Contains \Drupal\views_pgwslider\Plugin\views\style\ViewsPgwSlider.
 */

namespace Drupal\views_pgwslider\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
/**
 * Style plugin to render each item into PgwSlider.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "viewspgwslider",
 *   title = @Translation("Views PgwSlider"),
 *   help = @Translation("Displays rows as PgwSlider."),
 *   theme = "views_pgwslider_views",
 *   display_types = {"normal"}
 * )
 */
class ViewsPgwSlider extends StylePluginBase {

  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Set default options
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $settings = _views_pgwslider_default_settings();
    foreach ($settings as $k => $v) {
      $options[$k] = array('default' => $v);
    }
    return $options;
  }

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    //transitionEffect
    $form['transitionEffect'] = array(
      '#type' => 'select',
      '#options' => array(
        'fading' => $this->t('Fade'),
        'sliding' => $this->t('Slide'),
      ),
      '#title' => $this->t('Transition Effect'),
      '#default_value' => $this->options['transitionEffect'],
      '#description' => $this->t('You can choose between 2 different transition effects: "fading" or "sliding".'),
    );
    //selectionMode
    $form['selectionMode'] = array(
      '#type' => 'select',
      '#options' => array(
        'click' => $this->t('Click'),
        'mouseOver' => $this->t('MouseOver'),
      ),
      '#title' => $this->t('Thumbnail Selection Mode'),
      '#default_value' => $this->options['selectionMode'],
      '#description' => $this->t('Sets the selection mode to "Click" or "MouseOver". "MouseOver" only works with the transition effect "Fade".'),
    );
    //listPosition
    $form['listPosition'] = array(
      '#type' => 'select',
      '#options' => array(
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
      ),
      '#title' => $this->t('Thumbnail Position'),
      '#default_value' => $this->options['listPosition'],
      '#description' => $this->t('Sets the position of thumbnails to "Left" or "Right".'),
    );
    //autoSlide
    $form['autoSlide'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Auto Slide'),
      '#default_value' => $this->options['autoSlide'],
      '#description' => $this->t('Enable or disable the automatic transition between the slides.'),
    );
    //displayControls
    $form['displayControls'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Navigation Controls'),
      '#default_value' => $this->options['displayControls'],
      '#description' => $this->t('Previous and next controls to navigate the slides.'),
    );
    //touchControls
    $form['touchControls'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Touch Controls for mobile devices'),
      '#default_value' => $this->options['touchControls'],
      '#description' => $this->t('Touch controls for mobile devices to navigate the slides.'),
    );
    //verticalCentering
    $form['verticalCentering'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Vertical Centering'),
      '#default_value' => $this->options['verticalCentering'],
      '#description' => $this->t('If the height of the thumbnail list or the main container is smaller than the height of the image, this option can vertically center the element.'),
    );
    //adaptiveHeight
    $form['adaptiveHeight'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Adaptive Height'),
      '#default_value' => $this->options['adaptiveHeight'],
      '#description' => $this->t('If your images have a different height, this option adjusts automatically the global height of the slider.'),
    );
    //maxHeight
    $form['maxHeight'] = array(
      '#type' => 'number',
      '#title' => $this->t('Max Height'),
      '#default_value' => $this->options['maxHeight'],
      '#description' => $this->t('Enter a maximum height to your slider in pixels without the suffix "px".'),
    );
    //adaptiveDuration
    $form['adaptiveDuration'] = array(
      '#type' => 'number',
      '#title' => $this->t('Adaptive Duration'),
      '#default_value' => $this->options['adaptiveDuration'],
      '#description' => $this->t('This duration is the period in milliseconds, during the adjustment of the previous option runs.'),
    );
    //transitionDuration
    $form['transitionDuration'] = array(
      '#type' => 'number',
      '#title' => $this->t('Transition Duration'),
      '#default_value' => $this->options['transitionDuration'],
      '#description' => $this->t('Period of animation in milliseconds between 2 slides.'),
    );
    //intervalDuration
    $form['intervalDuration'] = array(
      '#type' => 'number',
      '#title' => $this->t('Transition Duration'),
      '#default_value' => $this->options['intervalDuration'],
      '#description' => $this->t('Interval in milliseconds before displaying of the next slide when "Auto Slide" option is activated.'),
    );
  }

}
