(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.viewspgwslider = {
        attach: function (context, settings) {
            $('.pgwSlider', context).each(function () {
                var $this = $(this);
                var $this_settings = $.parseJSON($this.attr('data-settings'));
                $this.pgwSlider($this_settings);
            });

        }
    };
})(jQuery, Drupal, drupalSettings);
